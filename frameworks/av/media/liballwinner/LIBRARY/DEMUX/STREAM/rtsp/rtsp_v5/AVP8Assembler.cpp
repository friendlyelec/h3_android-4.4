/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//#define LOG_NDEBUG 0
#define LOG_TAG "AVP8Assembler"
#include <utils/Log.h>
#include <CdxLog.h>

#include "AVP8Assembler.h"

#include "ARTPSource.h"
#include "ASessionDescription.h"

#include <media/stagefright/foundation/ABitReader.h>
#include <media/stagefright/foundation/ABuffer.h>
#include <media/stagefright/foundation/ADebug.h>
#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/foundation/hexdump.h>
#include <media/stagefright/Utils.h>

#include <ctype.h>
#include <stdint.h>

namespace android {

// static
AVP8Assembler::AVP8Assembler(
        const sp<AMessage> &notify, const AString &desc, const AString &params)
    : mNotifyMsg(notify),
      mParams(params),
      mAccessUnitRTPTime(0),
      mNextExpectedSeqNoValid(false),
      mNextExpectedSeqNo(0),
      mAccessUnitDamaged(false) {
    CDX_UNUSE(desc);
}

AVP8Assembler::~AVP8Assembler() {
}

ARTPAssembler::AssemblyStatus AVP8Assembler::addPacket(
        const sp<ARTPSource> &source) {
    List<sp<ABuffer> > *queue = source->queue();

    if (queue->empty()) {
        return NOT_ENOUGH_DATA;
    }

    if (mNextExpectedSeqNoValid) {
        List<sp<ABuffer> >::iterator it = queue->begin();
        while (it != queue->end()) {
            if ((uint32_t)(*it)->int32Data() >= mNextExpectedSeqNo) {
                break;
            }

            it = queue->erase(it);
        }

        if (queue->empty()) {
            return NOT_ENOUGH_DATA;
        }
    }

    sp<ABuffer> buffer = *queue->begin();

    if (!mNextExpectedSeqNoValid) {
        mNextExpectedSeqNoValid = true;
        mNextExpectedSeqNo = (uint32_t)buffer->int32Data();
    } else if ((uint32_t)buffer->int32Data() != mNextExpectedSeqNo) {
        ALOGV("Not the sequence number I expected");

        return WRONG_SEQUENCE_NUMBER;
    }

    uint32_t rtpTime;
    CHECK(buffer->meta()->findInt32("rtp-time", (int32_t *)&rtpTime));

    if (mPackets.size() > 0 && rtpTime != mAccessUnitRTPTime) {
        submitAccessUnit();
    }
    mAccessUnitRTPTime = rtpTime;

    /*
     * VP8 payload header
     *
     *        0 1 2 3 4 5 6 7
	 *       +-+-+-+-+-+-+-+-+
	 *       |X|R|N|S|PartID | (REQUIRED)
	 *       +-+-+-+-+-+-+-+-+
	 *    X: |I|L|T|K| RSV   | (OPTIONAL)
	 *       +-+-+-+-+-+-+-+-+
	 *    I: |M| PictureID   | (OPTIONAL)
	 *       +-+-+-+-+-+-+-+-+
	 *    L: |   TL0PICIDX   | (OPTIONAL)
	 *       +-+-+-+-+-+-+-+-+
	 *  T/K: |TID|Y| KEYIDX  | (OPTIONAL)
   	 *   	 +-+-+-+-+-+-+-+-+
     *
     */

    size_t bufOffset = 1;
    const uint8_t *data = buffer->data();

    CHECK_GE(buffer->size(), 1);

    //R MUST be set to zero.
    CHECK_EQ((data[0] & 0x40), 0);

    //PartID MUST NOT be larger than 8.
    CHECK_LE((data[0] & 0x0f), 8);

    if(data[0] & 0x80) {
    	//with extened control bits
    	CHECK_GE(buffer->size(), 2);

    	bufOffset += 1;
    	//RSV MUST be 0, although it's useless.
    	CHECK_EQ((data[1] & 0x0f), 0);

    	int32_t pictureID = data[1] & 0x80;
    	int32_t TL0PICIDX = data[1] & 0x40;
    	int32_t TID		  = data[1] & 0x20;
    	int32_t KEYIDX	  = data[1] & 0x10;

    	if(pictureID) {
    		bufOffset += 1;
    	}

    	if(TL0PICIDX) {
    		bufOffset += 1;
    		//If TL0PICIDX set to 1, TID MUST set to 1.
    		CHECK_EQ(TID, 1);
    	}

    	if(TID || KEYIDX) {
    		bufOffset += 1;
    	}
    }

    CHECK_LE(bufOffset, 4u);
    CHECK_LE(bufOffset, buffer->size());

    buffer->setRange(buffer->offset() + bufOffset, buffer->size() - bufOffset);

    mPackets.push_back(buffer);


    queue->erase(queue->begin());
    ++mNextExpectedSeqNo;

    return OK;
}

void AVP8Assembler::submitAccessUnit() {
    CHECK(!mPackets.empty());

    ALOGV("Access unit complete (%d nal units)", mPackets.size());

    sp<ABuffer> accessUnit;

    //if (mIsGeneric) //for Android NuPlayer

    accessUnit = MakeCompoundFromPackets(mPackets);


    if (mAccessUnitDamaged) {
        accessUnit->meta()->setInt32("damaged", true);
    }

    mPackets.clear();
    mAccessUnitDamaged = false;

    sp<AMessage> msg = mNotifyMsg->dup();
    msg->setBuffer("access-unit", accessUnit);
    msg->post();
}

ARTPAssembler::AssemblyStatus AVP8Assembler::assembleMore(
        const sp<ARTPSource> &source) {
    AssemblyStatus status = addPacket(source);
    if (status == MALFORMED_PACKET) {
        mAccessUnitDamaged = true;
    }
    return status;
}

void AVP8Assembler::packetLost() {
    CHECK(mNextExpectedSeqNoValid);
    ALOGV("packetLost (expected %d)", mNextExpectedSeqNo);

    ++mNextExpectedSeqNo;

    mAccessUnitDamaged = true;
}

void AVP8Assembler::onByeReceived() {
    sp<AMessage> msg = mNotifyMsg->dup();
    msg->setInt32("eos", true);
    msg->post();
}

}  // namespace android
