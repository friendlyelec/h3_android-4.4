#ifndef CEDARX_LOG_H
#define CEDARX_LOG_H

#ifdef __OS_ANDROID
#include <cutils/log.h>
#define CDX_LOGV(fmt, arg...) \
    ALOGV("\033[40;33m<%s:%u>"fmt"\033[0m", \
        strrchr(__FILE__, '/') + 1, __LINE__, ##arg)
    
#define CDX_LOGD(fmt, arg...) \
    ALOGD("\033[40;33m<%s:%u>"fmt"\033[0m", \
        strrchr(__FILE__, '/') + 1, __LINE__, ##arg)
    
#define CDX_LOGI(fmt, arg...) \
    ALOGI("\033[40;33m<%s:%u>"fmt"\033[0m", \
        strrchr(__FILE__, '/') + 1, __LINE__, ##arg)
    
#define CDX_LOGW(fmt, arg...) \
    ALOGW("\033[40;33m<%s:%u>"fmt"\033[0m", \
        strrchr(__FILE__, '/') + 1, __LINE__, ##arg)
    
#define CDX_LOGE(fmt, arg...) \
    ALOGE("\033[40;33m<%s:%u>"fmt"\033[0m", \
        strrchr(__FILE__, '/') + 1, __LINE__, ##arg)

#define CDX_BUF_DUMP(buf, len) \
    do { \
        char *_buf = (char *)buf;\
        char str[1024] = {0};\
        unsigned int index = 0, _len;\
        _len = (unsigned int)len;\
        snprintf(str, 1024, ":%d:[", _len);\
        for (index = 0; index < _len; index++)\
        {\
            snprintf(str + strlen(str), 1024, "0x%02hhx ", _buf[index]);\
        }\
        str[strlen(str) - 1] = ']';\
        CDX_LOGD("%s", str);\
    }while (0)
#else
#define CDX_LOGV(...) 
#define CDX_LOGD(...) 
#define CDX_LOGI(...) 
#define CDX_LOGW(...) 
#define CDX_LOGE(...) 
/*��������*/
#endif

#endif
