/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//#define LOG_NDEBUG 0
#define LOG_TAG "AAsfAssembler"
#include <utils/Log.h>
#include <CdxLog.h>

#include "AAsfAssembler.h"

#include "ARTPSource.h"

#include <media/stagefright/foundation/ABitReader.h>
#include <media/stagefright/foundation/ABuffer.h>
#include <media/stagefright/foundation/ADebug.h>
#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/foundation/hexdump.h>
#include <media/stagefright/foundation/base64.h>
#include <media/stagefright/Utils.h>

#include <ctype.h>
#include <stdint.h>

namespace android {

//#define SAVE_STREAM_DATA
// static
AAsfAssembler::AAsfAssembler(
        const sp<AMessage> &notify, const AString &desc, const AString &params)
    : mNotifyMsg(notify),
      mParams(params),
      mAccessUnitRTPTime(0),
      mNextExpectedSeqNoValid(false),
      mNextExpectedSeqNo(0),
      mAccessUnitDamaged(false),
      mMinPacketSize(0) {
	//decode asf header here

	  CDX_UNUSE(desc);
	  ssize_t startPos = mParams.find("base64,", 0);


	  if(startPos >= 0) {
		  AString asfHeader;
		  startPos += 7;

		  ssize_t endPos   = mParams.find(",", startPos);
		  if(endPos < 0) {
			  endPos = mParams.find(";", startPos);
			  if(endPos < 0) {
				  endPos = mParams.size();
			  }
		  }
		  CHECK_LE(startPos, endPos);
		  asfHeader.setTo(mParams.c_str(), startPos, endPos - startPos);
		  mHeaderData = decodeBase64(asfHeader);
	  } else {
		  TRESPASS();
	  }

	  startPos = mParams.find(";maxps=", 0);
	  if(startPos >= 0) {
		  startPos += 7;
		  char *end;
		  mMinPacketSize = strtoul(mParams.c_str() + startPos, &end, 10);
		  ALOGI("mMinPacketSize %u", mMinPacketSize);
	  }
}

AAsfAssembler::~AAsfAssembler() {
}


ARTPAssembler::AssemblyStatus AAsfAssembler::addPacket(
        const sp<ARTPSource> &source) {
    List<sp<ABuffer> > *queue = source->queue();

    if (queue->empty()) {
        return NOT_ENOUGH_DATA;
    }

    if (mNextExpectedSeqNoValid) {
        List<sp<ABuffer> >::iterator it = queue->begin();
        while (it != queue->end()) {
            if ((uint32_t)(*it)->int32Data() >= mNextExpectedSeqNo) {
                break;
            }

            it = queue->erase(it);
        }

        if (queue->empty()) {
            return NOT_ENOUGH_DATA;
        }
    }

    sp<ABuffer> buffer = *queue->begin();

    if (!mNextExpectedSeqNoValid) {
        mNextExpectedSeqNoValid = true;
        mNextExpectedSeqNo = (uint32_t)buffer->int32Data();
    } else if ((uint32_t)buffer->int32Data() != mNextExpectedSeqNo) {
        ALOGV("Not the sequence number I expected");
        mNextExpectedSeqNo = (uint32_t)buffer->int32Data();
//        return WRONG_SEQUENCE_NUMBER;
    }

    uint32_t offset = 0;
    if(buffer->meta()->findSize("offset", &offset)
    		&& !offset && !mPackets.empty()) {
    	//an integrated unit, submit it.
    	submitAccessUnit();
    }

    mPackets.push_back(buffer);

    queue->erase(queue->begin());
    ++mNextExpectedSeqNo;

    return OK;
}

void AAsfAssembler::submitAccessUnit() {
    CHECK(!mPackets.empty());

    ALOGV("Access unit complete (%d nal units)", mPackets.size());

    size_t totalSize = 0;
    size_t paddingSize = 0;

    for (List<sp<ABuffer> >::iterator it = mPackets.begin();
         it != mPackets.end(); ++it) {
    	totalSize += (*it)->size();

    }

    if(totalSize < mMinPacketSize) {
    	//need padding except the header data.
    	paddingSize = mMinPacketSize - totalSize;
    }

    if(mHeaderData != NULL) {
    	totalSize += mHeaderData->size();
    }

    sp<ABuffer> accessUnit = new ABuffer(totalSize + paddingSize);

    size_t offset = 0;

    if(mHeaderData != NULL) {
    	//add header data to first unit.
    	memcpy(accessUnit->data() + offset, mHeaderData->data(), mHeaderData->size());
    	offset += mHeaderData->size();
    	mHeaderData.clear();
    }

    for (List<sp<ABuffer> >::iterator it = mPackets.begin();
         it != mPackets.end(); ++it) {
        sp<ABuffer> nal = *it;
        memcpy(accessUnit->data() + offset, nal->data(), nal->size());
        offset += nal->size();
    }
    memset(accessUnit->data() + offset, 0, totalSize + paddingSize - offset);

    CopyTimes(accessUnit, *mPackets.begin());

    if (mAccessUnitDamaged) {
        accessUnit->meta()->setInt32("damaged", true);
    }

    mPackets.clear();
    mAccessUnitDamaged = false;
    sp<AMessage> msg = mNotifyMsg->dup();
    msg->setBuffer("access-unit", accessUnit);
    msg->post();
}

ARTPAssembler::AssemblyStatus AAsfAssembler::assembleMore(
        const sp<ARTPSource> &source) {
    AssemblyStatus status = addPacket(source);
    if (status == MALFORMED_PACKET) {
        mAccessUnitDamaged = true;
    }
    return status;
}

void AAsfAssembler::packetLost() {
    CHECK(mNextExpectedSeqNoValid);
    ALOGV("packetLost (expected %d)", mNextExpectedSeqNo);

    ++mNextExpectedSeqNo;

    mAccessUnitDamaged = true;
}

void AAsfAssembler::onByeReceived() {
    sp<AMessage> msg = mNotifyMsg->dup();
    msg->setInt32("eos", true);
    msg->post();
}

}  // namespace android
