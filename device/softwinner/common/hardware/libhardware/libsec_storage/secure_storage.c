/*
 * (C) Copyright 2007-2013
 * Allwinner Technology Co., Ltd. <www.allwinnertech.com>
 * Char <yanjianbo@allwinnertech.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <utils/Log.h>
#include <pthread.h> 


static pthread_mutex_t sec_mutex=PTHREAD_MUTEX_INITIALIZER;

/*
 *	commond parameters
 */
#define FLASH_TYPE_NAND		0
#define FLASH_TYPE_SD1  	1
#define FLASH_TYPE_SD2  	2
#define FLASH_TYPE_UNKNOW	-1

struct secblc_op_t{
	int item;
	unsigned char *buf;
	unsigned int len ;
};


#define SUNXI_SECSTORE_VERSION  1
/* Store element struct in nand/emmc */
#define MAX_STORE_LEN 0xc00 /*3K payload*/
#define STORE_OBJECT_MAGIC	0x17253948
#define STORE_REENCRYPT_MAGIC 0x86734716
#define STORE_WRITE_PROTECT_MAGIC 0x8ad3820f
typedef struct{
	unsigned int	magic ; /* store object magic*/
	int				id ;    /*store id, 0x01,0x02.. for user*/
	char			name[64]; /*OEM name*/
    unsigned int	re_encrypt; /*flag for OEM object*/
	unsigned int	version ;	
	unsigned int    write_protect ;  /*can be placed or not, =0, can be write_protectd*/
	unsigned int	reserved[3];
	unsigned int	actual_len ; /*the actual len in data buffer*/
	unsigned char	data[MAX_STORE_LEN]; /*the payload of secure object*/
	unsigned int	crc ; /*crc to check the sotre_objce valid*/
}store_object_t;


#define CMDLINE_FILE_PATH	("/proc/cmdline")

#define SEC_BLK_SIZE						(4096)
#define MAX_SECURE_STORAGE_MAX_ITEM          (32)
static unsigned char secure_storage_map[SEC_BLK_SIZE] = {0};
static unsigned int  secure_storage_inited = 0;

static unsigned int map_dirty;

static inline void set_map_dirty(void)   { map_dirty = 1; }
static inline void clear_map_dirty(void) { map_dirty = 0;}
static inline int try_map_dirty(void)    { return map_dirty ;}

static int flash_boot_type = FLASH_TYPE_UNKNOW;
/*
 * EMMC parameter
 */
#define SDMMC_SECTOR_SIZE				(512)
#define SDMMC_SECURE_STORAGE_START_ADD  (6*1024*1024/512)//6M
#define SDMMC_ITEM_SIZE                 (4*1024/512)//4K
static char *sd_oem_path="/dev/block/mmcblk0";


/*
 * Nand parameters
 */
#define SECBLK_READ                         _IO('V',20)
#define SECBLK_WRITE                        _IO('V',21)
#define SECBLK_IOCTL                        _IO('V',22)
static char *nand_oem_path="/dev/block/by-name/bootloader";

static int getInfoFromCmdline(char* key, char* value)
{
	char *p = NULL ;
	int line_len , key_len;
	int ret ;
	int fd;
	char cmdline[1024];
	
	memset(cmdline, 0, 1024);
	
	fd = open(CMDLINE_FILE_PATH, O_RDWR);
	if (!fd)
	{
		ALOGE("Failed to open %s", CMDLINE_FILE_PATH);
		return -1;	
	}

	ret = read(fd, cmdline, 1024);
	if(ret < 0 || ret >1024 ){
		ALOGE("Cmd line read fail\n");	
		close(fd);
		return -1;
	}
	close(fd);
	
	line_len = ret;
	key_len = strnlen(key, 1024);
	p=cmdline;

	while (p < cmdline + line_len - key_len)
	{
		if( strncmp(key, p, key_len) )
		{
			p++;
		}
		else
		{
			memcpy(value, p+key_len, 1);	
			return 0 ;
		}
	}

    strcpy(value, "-1");
    return -1;
}

static int get_flash_type(void)
{
	char ctype[16];

	memset(ctype, 0, 16);
	if( getInfoFromCmdline("boot_type=", ctype) ){
		ALOGE("Get boot type cmd line fail\n");
		return -1 ;
	}
	
	flash_boot_type = atol(ctype);
	ALOGD("Boot type %d\n", flash_boot_type);
	return 0;
}

/*nand secure storage read/write*/
static int _nand_read(int id, unsigned char *buf, ssize_t len)
{	
	int fd;
	int ret;
	struct secblc_op_t * secblk_op;
	
	if(!buf){
		ALOGE("-buf NULL\n");
		return -1;
	}
	if(id >MAX_SECURE_STORAGE_MAX_ITEM){
		ALOGE("out of range id %x\n", id);
		return -1;
	}
	fd = open(nand_oem_path, O_RDWR);
	if (!fd)
	{
		ALOGE("Failed to open %s", nand_oem_path);
		close(fd);
		return -1;	
	}
	
	secblk_op =(struct secblc_op_t *)malloc(sizeof(*secblk_op));
	if(!secblk_op){
		ALOGE("Out of memory\n");
		return -1 ;
	}

	secblk_op->item = id ;
	secblk_op->buf = (unsigned char *)buf;
	secblk_op->len = len ;

	ret = ioctl(fd, SECBLK_READ, (unsigned int)(secblk_op));
//	ALOGE(" ERR :boostup access sysfs fail,%s!\n",strerror(ret));
	free(secblk_op);
	close(fd);
	return  ret;		
}


static int _nand_write( int	id, unsigned char *buf, ssize_t len)
{	
	int fd;
	int ret;
	struct secblc_op_t * secblk_op;

	if(!buf){
		ALOGE("- buf NULL\n");
		return (-EINVAL);
	}

	if(id >MAX_SECURE_STORAGE_MAX_ITEM){
		ALOGE("out of range id %x\n", id);
		return (-EINVAL);
	}
	
	fd = open(nand_oem_path, O_RDWR);
	if (!fd)
	{
		ALOGE("Failed to open %s", nand_oem_path);
		return -1;	
	}

	secblk_op =(struct secblc_op_t *)malloc(sizeof(*secblk_op));
	if(!secblk_op){
		ALOGE("Out of memory\n");
		return -1 ;
	}

	secblk_op->item = id ;
	secblk_op->buf =(unsigned char *) buf;
	secblk_op->len = len ;
  
	ret = ioctl(fd, SECBLK_WRITE,secblk_op);	
	free(secblk_op);
	close(fd);
	return ret;
}

/*emmc secure storage read/write*/
static int _sd_read(int id, unsigned char *buf, ssize_t len)
{
	int offset ,ret ;
	char *align, *sd_align_buffer ;
	int fd;

	if(!buf){
		ALOGE("-buf NULL\n");
		return -1;
	}

	if(id >MAX_SECURE_STORAGE_MAX_ITEM){
		ALOGE("out of range id %x\n", id);
		return (-EINVAL);
	}

	sd_align_buffer = malloc(SEC_BLK_SIZE + 64);
	if(!sd_align_buffer){
		ALOGE("out of memory\n");
		return - ENOMEM;
	}

	align = (char *)(((unsigned int)sd_align_buffer+0x20)&(~0x1f));

	offset = (SDMMC_SECURE_STORAGE_START_ADD+SDMMC_ITEM_SIZE*2*id) *SDMMC_SECTOR_SIZE;

	fd = open(sd_oem_path, O_RDWR);
	if (!fd)
	{
		ALOGE("Failed to open %s", sd_oem_path);
		return -1;	
	}
	ret = lseek(fd, offset, SEEK_SET);
	if (ret < 0)
	{
		ALOGE("Failed to sleek %s, offset = %d\n", sd_oem_path, offset);
		return -1;	
	}
	ret = read(fd, align, len);
	if(ret != len){
		ALOGE("_sst_user_read: read request len 0x%x, actual read 0x%x\n", len, ret);
		free(sd_align_buffer);
		close(fd);
		return -1;
	}
	memcpy(buf, align, len);
	
	free(sd_align_buffer);
	close(fd);
	return 0 ;
}

static int _sd_write( int id, unsigned char *buf, ssize_t len)
{
	int offset, ret ; 
	char *align, *sd_align_buffer ;
	int fd;

	if(!buf){
		ALOGE("- buf NULL\n");
		return (-EINVAL);
	}

	if(id >MAX_SECURE_STORAGE_MAX_ITEM){
		ALOGE("out of range id %x\n", id);
		return (-EINVAL);
	}

	sd_align_buffer = malloc(SEC_BLK_SIZE + 64);
	if(!sd_align_buffer){
		ALOGE("out of memory\n");
		return - ENOMEM;
	}

	align = (char *)(((unsigned int)sd_align_buffer+0x20)&(~0x1f));
	memcpy(align, buf, len);

	offset = (SDMMC_SECURE_STORAGE_START_ADD+SDMMC_ITEM_SIZE*2*id)*SDMMC_SECTOR_SIZE;

	fd = open(sd_oem_path, O_RDWR);
	if (!fd)
	{
		ALOGE("Failed to open %s", sd_oem_path);
		return -1;	
	}
	
	ret = lseek(fd, offset, SEEK_SET);
	if (ret < 0)
	{
		ALOGE("Failed to sleek %s, offset = %d\n", sd_oem_path, offset);
		return -1;	
	}
	
	ret =  write(fd, align, len);
	if(ret != len){
		ALOGE("_sst_user_write: write request len 0x%x, actual write 0x%x\n",
				len, ret);
		free(sd_align_buffer);
		close(fd);
		return -1;
	}
	free(sd_align_buffer);
	close(fd);
	return 0 ;
}

static int nv_read( int	id, unsigned char *buf, ssize_t len)
{
	int ret ;
	switch(flash_boot_type){
		case FLASH_TYPE_NAND:
			ret = _nand_read(id, buf, len);
			break;
		case FLASH_TYPE_SD1:
		case FLASH_TYPE_SD2:
			ret = _sd_read(id, buf, len); 
			break;
		default:
			ALOGE("Unknown no-volatile device\n");
			ret = -1 ;
			break; 
	}

	return ret ;
}

static int nv_write( int id,unsigned char *buf, ssize_t len )
{
	int ret ;
	switch(flash_boot_type){
		case FLASH_TYPE_NAND:
			ret = _nand_write(id, buf, len);
			break;
		case FLASH_TYPE_SD1:
		case FLASH_TYPE_SD2:
			ret = _sd_write(id, buf,len);
			break;
		default:
			ALOGE("Unknown no-volatile device\n");
			ret = -1 ;
			break; 
	}
	return ret ;
}

/*Low-level operation*/
static int sunxi_secstorage_read(int item, unsigned char *buf, unsigned int len)
{
	return nv_read(item, buf, len);
}

static int sunxi_secstorage_write(int item, unsigned char *buf, unsigned int len)
{
	return nv_write(item, buf, len);
}

/*
 * Map format:
 *		name:length\0
 *		name:length\0
 */
static int __probe_name_in_map(unsigned char *buffer, const char *item_name, int *len)
{
	unsigned char *buf_start = buffer;
	int   index = 1;
	char  name[64], length[32];
	int   i,j, name_len ;

	ALOGD("__probe_name_in_map\n");

	while(*buf_start != '\0')
	{
		memset(name, 0, 64);
		memset(length, 0, 32);
		i=0;
		while(buf_start[i] != ':')
		{
			name[i] = buf_start[i];
			i ++;
		}
		name_len=i ;
		i ++;j=0;
		while( (buf_start[i] != ' ') && (buf_start[i] != '\0') )
		{
			length[j] = buf_start[i];
			i ++;j++;
		}

		if(memcmp(item_name, name, name_len) ==0 )
		{
			buf_start += strlen(item_name) + 1;
			*len = atol((const char *)length);
			return index;
		}
		index ++;
		buf_start += strlen((const char *)buf_start) + 1;
	}

	return -1;
}

static int __fill_name_in_map(unsigned char *buffer, const char *item_name, int length)
{
	unsigned char *buf_start = buffer;
	int   index = 1;
	int   name_len;

	while(*buf_start != '\0')
	{

		name_len = 0;
		while(buf_start[name_len] != ':')
			name_len ++;
		if(!memcmp((const char *)buf_start, item_name, name_len))
		{
			ALOGD("name in map %s\n", buf_start);
			return index;
		}
		index ++;
		buf_start += strlen((const char *)buf_start) + 1;
	}
	if(index >= 32)
		return -1;

	sprintf((char *)buf_start, "%s:%d", item_name, length);

	return index;
}

static int __discard_name_in_map(unsigned char *buffer, const char *item_name)
{
	unsigned char *buf_start = buffer, *last_start;
	int   index = 1;
	int   name_len;

	while(*buf_start != '\0')
	{

		name_len = 0;
		while(buf_start[name_len] != ':')
			name_len ++;
		if(!memcmp((const char *)buf_start, item_name, name_len))
		{
			ALOGE("name in map %s\n", buf_start);
			last_start = buf_start + strlen((const char *)buf_start) + 1;
			if(*last_start == '\0')
			{
				memset(buf_start, 0, strlen((const char *)buf_start));
			}
			else
			{
				memcpy(buf_start, last_start, 4096 - (last_start - buffer));
			}

			return index;
		}
		index ++;
		buf_start += strlen((const char *)buf_start) + 1;
	}

	return -1;
}
/*load source data to secure_object struct
 *
 * src		: secure_object 
 * len		: secure_object buffer len 
 * payload	: taregt payload 
 * retLen	: target payload actual length
 * */
static int unwrap_secure_object(void * src,  unsigned int len, void * payload,   int *retLen )
{
	store_object_t *obj;

	if(len != sizeof(store_object_t)){
		ALOGE("Input length not equal secure object size 0x%x\n",len);
		return -1 ;
	}

	obj = (store_object_t *) src ;

	if( obj->magic != STORE_OBJECT_MAGIC ){
		ALOGE("Input object magic fail [0x%x]\n", obj->magic);
		return -1 ;
	}
	
	if( obj->re_encrypt == STORE_REENCRYPT_MAGIC){
		ALOGD("secure object is encrypt by chip\n");
	}

	if( obj->crc != crc32( 0 , (void *)obj, sizeof(*obj)-4 ) ){
		ALOGE("Input object crc fail [0x%x]\n", obj->crc);
		return -1 ;
	}

	memcpy(payload, obj->data ,obj->actual_len);
	*retLen = obj->actual_len ;

	return 0 ;
}

/*Store source data to secure_object struct
 *
 * src		: payload data to secure_object
 * name		: input payloader data name
 * tagt		: taregt secure_object
 * len		: input payload data length
 * retLen	: target secure_object length
 * */
static int wrap_secure_object(void * src, const char *name,  unsigned int len, void * tagt,  unsigned int *retLen)
{
	store_object_t *obj;

	if(len >MAX_STORE_LEN){
		ALOGE("Input length larger then secure object payload size\n");
		return -1 ;
	}

	obj = (store_object_t *) tagt ;
	*retLen= sizeof( store_object_t );

	obj->magic = STORE_OBJECT_MAGIC ;
	strncpy( obj->name, name, 64 );
	obj->re_encrypt = 0 ;
	obj->version = SUNXI_SECSTORE_VERSION;
	obj->id = 0;
	obj->write_protect = 0 ;
	memset(obj->reserved, 0, 4 );
	obj->actual_len = len ;
	memcpy( obj->data, src, len);
	
	obj->crc = crc32(0 , (const unsigned char *)obj, sizeof(*obj)-4 );
	
	return 0 ;
}

static int sunxi_secure_storage_read(const char *item_name, char *buffer, int buffer_len, int *data_len)
{
	int ret, index;
	int len_in_store;
	unsigned char * buffer_to_sec ;

	ALOGD("secure storage read %s \n", item_name);
	if(!secure_storage_inited)
	{
		ALOGE("%s err: secure storage has not been inited\n", __func__);
		return -1;
	}
	
	buffer_to_sec = (unsigned char *)malloc(SEC_BLK_SIZE);
	if(!buffer_to_sec ){
		ALOGE("%s out of memory",__func__);
		return -1;
	}

	index = __probe_name_in_map(secure_storage_map, item_name, &len_in_store);
	if(index < 0)
	{
		ALOGE("no item name %s in the map\n", item_name);
		free(buffer_to_sec);
		return -1;
	}
	memset(buffer, 0, buffer_len);
	ret = sunxi_secstorage_read(index, buffer_to_sec, SEC_BLK_SIZE);
	if(ret)
	{
		ALOGE("read secure storage block %d name %s err\n", index, item_name);
		free(buffer_to_sec);
		return -1;
	}
	if(len_in_store > buffer_len)
	{
		memcpy(buffer, buffer_to_sec, buffer_len);
	}
	else
	{
		memcpy(buffer, buffer_to_sec, len_in_store);
	}
	*data_len = len_in_store;

	ALOGD("secure storage read %s done\n",item_name);

	free(buffer_to_sec);
	return 0;
}

/*Add new item to secure storage*/
static int sunxi_secure_storage_write(const char *item_name, char *buffer, int length)
{
	int ret, index;

	if(!secure_storage_inited)
	{
		ALOGE("%s err: secure storage has not been inited\n", __func__);

		return -1;
	}
	index = __fill_name_in_map(secure_storage_map, item_name, length);
	if(index < 0)
	{
		ALOGE("write secure storage block %d name %s overrage\n", index, item_name);

		return -1;
	}

	ret = sunxi_secstorage_write(index, (unsigned char *)buffer, SEC_BLK_SIZE);
	if(ret)
	{
		ALOGE("write secure storage block %d name %s err\n", index, item_name);

		return -1;
	}
	set_map_dirty();
	ALOGE("write secure storage: %d ok\n", index);

	return 0;
}

static int sunxi_secure_storage_probe(const char *item_name)
{
	int ret;
	int len;

	if(!secure_storage_inited)
	{
		ALOGE("%s err: secure storage has not been inited\n", __func__);

		return -1;
	}
	ret = __probe_name_in_map(secure_storage_map, item_name, &len);
	if(ret < 0)
	{
		ALOGE("no item name %s in the map\n", item_name);

		return -1;
	}
	return 0 ;
}

static int sunxi_secure_storage_erase(const char *item_name)
{
	int ret, index;
	unsigned char  buffer[4096];

	if(!secure_storage_inited)
	{
		ALOGE("%s err: secure storage has not been inited\n", __func__);

		return -1;
	}
	index = __discard_name_in_map(secure_storage_map, item_name);
	if(index < 0)
	{
		ALOGE("no item name %s in the map\n", item_name);

		return -2;
	}
	memset(buffer, 0xff, 4096);
	ret = sunxi_secstorage_write(index, buffer, 4096);
	if(ret<0)
	{
		ALOGE("erase secure storage block %d name %s err\n", index, item_name);

		return -1;
	}
	set_map_dirty();
	ALOGE("erase secure storage: %d ok\n", index);

	return 0;
}


int sunxi_secure_object_read(const char *item_name, char *buffer, int buffer_len, int *data_len)
{
	char secure_object[4096];
	int retLen ,ret ;

	memset(secure_object, 0, 4096);

	ret = sunxi_secure_storage_read(item_name, secure_object, 4096, &retLen);
	if(ret){
		ALOGE("sunxi storage read fail\n");
		return -1 ;
	}

	return unwrap_secure_object(secure_object, retLen, buffer, data_len);
}

int sunxi_secure_object_write(const char *item_name, char *buffer, int length)
{
	char secure_object[4096];
	unsigned int retLen ;
	int ret;

	/*
	 * If there is THE same name itme in the secure storage, we need to decide
	 * how to deal with it.
	 * case 1. The same name in the secure storage is write_protected, Do thing.
	 * case 2. Otherwise, Erase it and write it again.
	 */ 
	if( sunxi_secure_storage_probe(item_name) == 0 ) {
		/*the same name in map*/
		sunxi_secure_object_read(item_name, secure_object, 4096, &retLen);	
		store_object_t * so = (store_object_t *)secure_object ;	
		if( so->magic == STORE_OBJECT_MAGIC && \
			so->write_protect  == STORE_WRITE_PROTECT_MAGIC ){
			ALOGE("Can't write a write_protect secure item\n");
			return -1 ;
		}else{
			ALOGE("secure object name[%s] already in device\n",item_name);
//			if( sunxi_secure_storage_erase(item_name) <0 ){
//				ALOGE("Erase the item %s which already in secure storage fail\n",item_name);
//				return -1 ;	
//			}
		}
	}

	memset(secure_object, 0, 4096);
	retLen = 0 ;
	ret = wrap_secure_object((void *)buffer, item_name, length,secure_object,&retLen);
	if(ret <0 || retLen >4096){
		ALOGE("wrap fail before secure storage write\n");
		return -1 ;
	}
	return sunxi_secure_storage_write(item_name, secure_object, retLen);
}


int sunxi_secure_storage_init(void)
{
	int ret;

	pthread_mutex_lock(&sec_mutex);

	if(!secure_storage_inited)
	{
		get_flash_type();
		ret = sunxi_secstorage_read(0, secure_storage_map, SEC_BLK_SIZE);
		if(ret < 0)
		{
			ALOGE("get secure storage map err\n");
			pthread_mutex_unlock(&sec_mutex);
			return -1;
		}
		else if(ret > 0)
		{
			ALOGD("the secure storage map is empty\n");
			memset(secure_storage_map, 0, SEC_BLK_SIZE);
		}
	}

	secure_storage_inited = 1;
	return 0;
}

int sunxi_secure_storage_exit(int mode)
{
	int ret;
	if (!secure_storage_inited)
	{
		ALOGE("err: secure storage has not been inited\n");
		return -1;	
	}
	if (mode || try_map_dirty())
	{
		ret = sunxi_secstorage_write(0, secure_storage_map, SEC_BLK_SIZE);
		if(ret<0)
		{
			printf("write secure storage map\n");
			pthread_mutex_unlock(&sec_mutex);
			 return -1;	
		}
	}
	secure_storage_inited = 0;
	clear_map_dirty();
	pthread_mutex_unlock(&sec_mutex);

	return 0;
}
